package com.github.dimonekun.example.strings.service

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(value =  SpringRunner::class)
@SpringBootTest
class StringsServiceApplicationTest {

    @Test
    fun contextLoads() {

    }
}