package com.github.dimonekun.example.strings.service

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLOutputFactory

@SpringBootApplication
class StringsServiceApplication {
    @Bean
    fun xmlInputFactory(): XMLInputFactory {
        return XMLInputFactory.newInstance()
    }

    @Bean
    fun xmlOutputFactory(): XMLOutputFactory {
        return XMLOutputFactory.newInstance()
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(StringsServiceApplication::class.java, *args)
}