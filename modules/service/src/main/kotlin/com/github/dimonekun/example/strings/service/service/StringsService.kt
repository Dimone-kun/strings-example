package com.github.dimonekun.example.strings.service.service

import com.github.dimonekun.example.strings.service.repository.StringEntry
import java.io.InputStream
import java.io.OutputStream

interface StringsService {

    fun import(stream: InputStream)

    fun export(outputStream: OutputStream)

    fun getAll(): Iterable<StringEntry>

    fun getByName(name: String): StringEntry?

    fun existWithName(name: String): Boolean

    fun save(entry: StringEntry): StringEntry
}