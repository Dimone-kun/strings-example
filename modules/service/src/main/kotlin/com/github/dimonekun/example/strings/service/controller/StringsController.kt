package com.github.dimonekun.example.strings.service.controller

import com.github.dimonekun.example.strings.service.repository.StringEntry
import com.github.dimonekun.example.strings.service.service.StringsService
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletResponse

@RestController
class StringsController(@Autowired val service: StringsService) {
    @PostMapping("upload")
    fun upload(@RequestParam("file") file: MultipartFile) {
        service.import(file.inputStream)
    }

    @GetMapping("download")
    fun download(response: HttpServletResponse) {
        response.contentType = "text/xml"
        service.export(response.outputStream)
    }

    @GetMapping("string")
    @ResponseBody
    fun getAll(): Iterable<StringEntry> {
        return service.getAll()
    }

    @GetMapping("string/{name}")
    @ResponseBody
    fun getByName(@PathVariable("name") stringName: String): StringEntry {
        return service.getByName(stringName) ?: throw NotFound()
    }

    @PutMapping(path = ["string/{name}"], consumes = ["text/plain"])
    fun updateStringText(@PathVariable("name") stringName: String, @RequestBody stringValue: String) {
        val entry = service.getByName(stringName) ?: throw NotFound()

        entry.value = stringValue
        service.save(entry)
    }

    @PutMapping(path = ["string/{name}"], consumes = ["application/json"])
    fun updateStringJson(@PathVariable("name") stringName: String, @RequestBody entryDto: StringEntryDto) {
        val entry = service.getByName(stringName) ?: throw NotFound()
        entryDto.toEntry(entry)
        service.save(entry)
    }

    @PostMapping(path = ["string"], consumes = ["application/json"])
    @ResponseBody
    fun createString(@RequestBody entryDto: StringEntryDto): StringEntry {
        return createNewString(entryDto)
    }

    @PostMapping(path = ["string/{name}"], consumes = ["application/json"])
    @ResponseBody
    fun createString(@PathVariable("name") stringName: String, @RequestBody entryDto: StringEntryDto): StringEntry {
        entryDto.name = stringName

        return createNewString(entryDto)
    }

    @PostMapping(path = ["string/{name}"], consumes = ["text/plain"])
    @ResponseBody
    fun createStringText(@PathVariable("name") stringName: String, @RequestBody stringValue: String): StringEntry {
        return createNewString(StringEntryDto(stringName, stringValue))
    }

    private fun createNewString(entryDto: StringEntryDto): StringEntry {
        if (entryDto.name == null) {
            throw MissingArgumentException("name is null")
        }
        if (service.existWithName(entryDto.name!!)) {
            throw AlreadyExists()
        }
        return service.save(entryDto.toEntry())
    }
}

final data class StringEntryDto(var name: String?, val value: String?) {
    constructor(): this(null, null)

    fun toEntry(): StringEntry {
        return StringEntry(
                name ?: throw MissingArgumentException("name is null"),
                value ?: throw IllegalStateException("value is null")
        )
    }

    fun toEntry(entry: StringEntry) {
        if (value != null) {
            entry.value = value
        }
    }
}

@ResponseStatus(code = HttpStatus.NOT_FOUND)
final class NotFound : RuntimeException()

@ResponseStatus(code = HttpStatus.CONFLICT)
final class AlreadyExists : RuntimeException()

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
final class MissingArgumentException(message: String) : RuntimeException(message)