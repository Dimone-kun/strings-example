package com.github.dimonekun.example.strings.service.service

import com.github.dimonekun.example.strings.service.repository.StringEntry
import com.github.dimonekun.example.strings.service.repository.StringEntryRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.atLeast
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.io.ByteArrayOutputStream
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLOutputFactory

@RunWith(SpringRunner::class)
class StringServiceImplTest {
    lateinit var service: StringsService

    @MockBean
    lateinit var repository: StringEntryRepository

    @Before
    fun init() {
        service = StringsServiceImpl(repository, XMLInputFactory.newInstance(), XMLOutputFactory.newInstance())
    }

    @Test
    fun saveTest() {
        whenever(repository.save(any<StringEntry>())).then { invocation ->  invocation.arguments[0]}

        service.save(StringEntry("Preferences", "2333"))
        verify(repository).save(StringEntry("Preferences", "2333"))
    }

    @Test
    fun getAllTest() {
        whenever(repository.findAll()).thenReturn(listOf(
                StringEntry("Preferences", "2333"),
                StringEntry("FeedBack", "Feedback")
        ))

        val result = service.getAll()

        Assert.assertTrue(result.count() == 2)
        Assert.assertTrue(result.contains(StringEntry("Preferences", "2333")))
        Assert.assertTrue(result.contains(StringEntry("FeedBack", "Feedback")))
    }

    @Test
    fun getByNameTest() {
        whenever(repository.findOne("Preferences")).thenReturn(StringEntry("Preferences", "2333"))

        val entry = service.getByName("Preferences")
        Assert.assertEquals(StringEntry("Preferences", "2333"), entry)
    }

    @Test
    fun getByNameNotFoundTest() {
        whenever(repository.findOne(any())).thenReturn(null)
        Assert.assertNull(service.getByName("Preferences"))
    }

    @Test
    fun existsWithNameTest() {
        whenever(repository.exists("Preferences")).thenReturn(true)

        Assert.assertTrue(service.existWithName("Preferences"))
    }

    @Test
    fun existsWithNameNotFoundTest() {
        whenever(repository.exists(any())).thenReturn(false)
        Assert.assertFalse(service.existWithName("Preferences"))
    }

    @Test
    fun exportTest() {
        whenever(repository.findAll()).thenReturn(listOf(
                StringEntry("Preferences", "2333"),
                StringEntry("FeedBack", "Feedback")
        ))
        val reader = ByteArrayOutputStream()
        service.export(reader)
        val inputAsString = reader.toString("UTF-8")
        Assert.assertTrue(inputAsString.contains("<string name=\"Preferences\">2333</string>"))
        Assert.assertTrue(inputAsString.contains("<string name=\"FeedBack\">Feedback</string>"))
    }

    @Test
    fun importTest() {
        val testFile = javaClass.getResourceAsStream("/strings.xml")!!
        service.import(testFile)
        verify(repository, atLeast(19)).save(any<StringEntry>())
    }
}