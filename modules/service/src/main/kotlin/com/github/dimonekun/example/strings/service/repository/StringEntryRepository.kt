package com.github.dimonekun.example.strings.service.repository

import org.springframework.data.repository.CrudRepository

interface StringEntryRepository : CrudRepository<StringEntry, String>

data class StringEntry(val name:String, var value: String)