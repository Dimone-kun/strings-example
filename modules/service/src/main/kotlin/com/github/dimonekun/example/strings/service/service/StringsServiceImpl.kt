package com.github.dimonekun.example.strings.service.service

import com.github.dimonekun.example.strings.service.repository.StringEntry
import com.github.dimonekun.example.strings.service.repository.StringEntryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.InputStream
import java.io.OutputStream
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLOutputFactory
import javax.xml.stream.XMLStreamException
import javax.xml.stream.XMLStreamReader

@Service
class StringsServiceImpl(@Autowired val repository: StringEntryRepository,
                         @Autowired private val xmlInputFactory: XMLInputFactory,
                         @Autowired private val xmlOutputFactory: XMLOutputFactory) : StringsService {

    override fun import(stream: InputStream) {
        val reader = xmlInputFactory.createXMLStreamReader(stream)
        try {
            while (reader.hasNext()) {
                if (XMLStreamReader.START_ELEMENT == reader.next() && "resources" == reader.localName){
                    readResources(reader)
                }
            }
        } finally {
            reader.close()
            stream.close()
        }
    }

    private fun readResources(reader: XMLStreamReader) {
        while (reader.hasNext()) {
            val xmlEvent = reader.next()
            if (XMLStreamReader.END_ELEMENT == xmlEvent && "resources" == reader.localName) {
                return
            } else if (XMLStreamReader.START_ELEMENT == xmlEvent && "string" == reader.localName) {
                repository.save(readStringEntry(reader))
            }
        }
        throw XMLStreamException("Premature end of file")
    }

    private fun readStringEntry(reader: XMLStreamReader) : StringEntry {
        return StringEntry(
                reader.getAttributeValue(null, "name"),
                reader.elementText
        )
    }

    override fun export(outputStream: OutputStream) {
        val writer = xmlOutputFactory.createXMLStreamWriter(outputStream, "UTF-8")
        try {
            writer.writeStartDocument("UTF-8", "1.0")
            writer.writeStartElement("resources")
            for (stringEntry in repository.findAll()) {
                writer.writeStartElement("string")
                writer.writeAttribute("name", stringEntry.name)
                writer.writeCharacters(stringEntry.value)
                writer.writeEndElement()
            }
            writer.writeEndElement()
            writer.writeEndDocument()
        } finally {
            writer.close()
        }
    }

    override fun getAll(): Iterable<StringEntry> {
        return repository.findAll()
    }

    override fun getByName(name: String): StringEntry? {
        return repository.findOne(name)
    }

    override fun existWithName(name: String): Boolean {
        return repository.exists(name)
    }

    override fun save(entry: StringEntry): StringEntry {
        return repository.save(entry)
    }
}