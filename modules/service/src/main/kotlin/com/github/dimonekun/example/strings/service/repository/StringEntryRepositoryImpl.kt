package com.github.dimonekun.example.strings.service.repository

import org.springframework.stereotype.Repository
import java.util.*

@Repository
class StringEntryRepositoryImpl : StringEntryRepository {

    private val entriesMap = LinkedHashMap<String, StringEntry>()

    override fun <S : StringEntry?> save(entity: S): S {
        if (entity == null) {
            throw IllegalArgumentException()
        }

        entriesMap.put(entity.name, entity)
        return entity
    }

    override fun <S : StringEntry?> save(entities: MutableIterable<S>?): MutableIterable<S>? {
        entities?.filter { it != null }?.forEach { entriesMap.put(it!!.name, it) }
        return entities
    }

    override fun findOne(id: String?): StringEntry? {
        return entriesMap[id]
    }

    override fun findAll(): MutableIterable<StringEntry> {
        return entriesMap.values
    }

    override fun findAll(ids: MutableIterable<String>?): MutableIterable<StringEntry> {
        if (ids == null) {
            return Collections.emptyList()
        }

        return entriesMap.filter { entry: Map.Entry<String, StringEntry> -> ids.contains(entry.key) }.values.toMutableList()
    }

    override fun count(): Long {
        return entriesMap.size.toLong()
    }

    override fun exists(id: String?): Boolean {
        return entriesMap.containsKey(id)
    }

    override fun deleteAll() {
        entriesMap.clear()
    }

    override fun delete(id: String?) {
        entriesMap.remove(id)
    }

    override fun delete(entity: StringEntry?) {
        if (entity != null) {
            entriesMap.remove(entity.name)
        }
    }

    override fun delete(entities: MutableIterable<StringEntry>?) {
        entities?.map { it.name }?.forEach { entriesMap.remove(it) }
    }
}