package com.github.dimonekun.example.strings.service.controller

import com.github.dimonekun.example.strings.service.repository.StringEntry
import com.github.dimonekun.example.strings.service.service.StringsService
import com.nhaarman.mockitokotlin2.*
import org.hamcrest.core.Is
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.io.ByteArrayInputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets

@RunWith(SpringRunner::class)
@WebMvcTest(StringsController::class)
class StringsControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var stringsService: StringsService

    @Test
    fun uploadTest() {
        val file = MockMultipartFile("file", "strings.xml", MediaType.TEXT_XML.type,
                this.javaClass.getResourceAsStream("strings.xml"))
        mvc.perform(multipart("/upload").file(file))
                .andExpect(status().isOk)

        verify(stringsService, atLeastOnce()).import(any())
    }

    @Test
    fun downloadTest() {
        val mockString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<resources>" +
                "<string name=\"Preferences\">2333</string>" +
                "<string name=\"FeedBack\">Feedback</string>" +
                "</resources>"

        whenever(stringsService.export(any())).then { invocation ->
            val outputStream = invocation.getArgument<OutputStream>(0)
            outputStream.write(mockString.toByteArray(Charsets.UTF_8))
        }

        mvc.perform(get("/download"))
                .andExpect(status().isOk)
                .andExpect(content().string(mockString))
    }

    @Test
    fun getAllTest() {
        whenever(stringsService.getAll()).thenReturn(listOf(
                StringEntry("Preferences", "2333"),
                StringEntry("FeedBack", "Feedback")
        ))
        putMockEntry(StringEntry("Preferences", "2333"))
        putMockEntry(StringEntry("FeedBack", "Feedback"))

        mvc.perform(get("/string").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].name", Is.`is`("Preferences")))
                .andExpect(jsonPath("$[0].value", Is.`is`("2333")))
                .andExpect(jsonPath("$[1].name", Is.`is`("FeedBack")))
                .andExpect(jsonPath("$[1].value", Is.`is`("Feedback")))
    }

    @Test
    fun getSingleTest() {
        putMockEntry(StringEntry("Preferences", "2333"))

        mvc.perform(get("/string/Preferences"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("name", Is.`is`("Preferences")))
                .andExpect(jsonPath("value", Is.`is`("2333")))
    }

    @Test
    fun getSingle404Test() {
        whenever(stringsService.getByName(any())).thenReturn(null)

        mvc.perform(get("/string/Preference"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun updateStringTextTest() {
        putMockEntry(StringEntry("Preferences", "2333"))

        mvc.perform(put("/string/Preferences").contentType(MediaType.TEXT_PLAIN).content("666"))
                .andExpect(status().isOk)

        verify(stringsService).save(StringEntry("Preferences", "666"))
    }

    @Test
    fun updateStringJsonTest() {
        putMockEntry(StringEntry("Preferences", "2333"))

        mvc.perform(put("/string/Preferences").contentType(MediaType.APPLICATION_JSON)
                .content("""{"value":"666"}"""))
                .andExpect(status().isOk)

        verify(stringsService).save(StringEntry("Preferences", "666"))
    }

    @Test
    fun updateStringText404Test() {
        whenever(stringsService.getByName(any())).thenReturn(null)

        mvc.perform(put("/string/Preferences").contentType(MediaType.TEXT_PLAIN).content("666"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun updateStringJson404Test() {
        whenever(stringsService.getByName(any())).thenReturn(null)

        mvc.perform(put("/string/Preferences").contentType(MediaType.APPLICATION_JSON)
                .content("""{"name":"Preferences", "value":"666"}"""))
                .andExpect(status().isNotFound)
    }

    @Test
    fun createStringTest() {
        mvc.perform(post("/string").contentType(MediaType.APPLICATION_JSON)
                .content("""{"name":"Preferences", "value":"666"}"""))
                .andExpect(status().isOk)

        verify(stringsService).save(StringEntry("Preferences", "666"))
    }

    @Test
    fun createStringByNameTest() {
        mvc.perform(post("/string/Preferences").contentType(MediaType.APPLICATION_JSON)
                .content("""{"value":"666"}"""))
                .andExpect(status().isOk)

        verify(stringsService).save(StringEntry("Preferences", "666"))
    }

    @Test
    fun createStringByNameTextRequestTest() {
        mvc.perform(post("/string/Preferences").contentType(MediaType.TEXT_PLAIN)
                .content("666"))
                .andExpect(status().isOk)

        verify(stringsService).save(StringEntry("Preferences", "666"))
    }

    @Test
    fun createStringConflictTest() {
        putMockEntry(StringEntry("Preferences", "2333"))

        mvc.perform(post("/string").contentType(MediaType.APPLICATION_JSON)
                .content("""{"name":"Preferences", "value":"666"}"""))
                .andExpect(status().isConflict)

        verify(stringsService, never()).save(any())
    }

    private fun putMockEntry(entry: StringEntry) {
        whenever(stringsService.getByName(entry.name)).thenReturn(entry)
        whenever(stringsService.existWithName(entry.name)).thenReturn(true)
    }
}